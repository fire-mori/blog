FROM node:12
COPY . /app
WORKDIR /app
RUN npm run build
WORKDIR /app/server
EXPOSE 3000
CMD node server.js
