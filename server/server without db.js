const express = require('express');
const moment = require('moment');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, '../build')));
app.use(express.json());
app.get('/ping', (req, res) => {
  return res.send('pong');
});

let posts = null;
var fs = require('fs');

fs.readFile('./data/dummy-posts.json', 'utf8', (err, contents) => {
  posts = JSON.parse(contents);
});

function postDate() {
  return moment().format('DD/MM/YYYY');
}

app.get('/api/posts', (req, res) => {
  res.send(posts);
})

app.post('/api/posts', (req, res) => { // create new
  const newPost = {
    id: posts.length + 1,
    title: req.body.title,
    excerpt: req.body.excerpt,
    image: req.body.image,
    date: postDate(),
  }
  posts.push(newPost);
  res.send(newPost);
})

app.get('/api/posts/:id', (req, res) => {
  const id = String(req.params.id);
  const post = posts.find(post => post.id === id);
  if (!post) return res.status(404).send('The post with the given ID was not found.');
  res.send(post);
})

app.patch('/api/posts/:id', (req, res) => { // update by ID
  const id = String(req.params.id);
  const post = posts.find(post => post.id === id);
  if (!post) return res.status(404).send('The post with the given ID was not found.');
  const changes = {
    title: req.body.title,
    excerpt: req.body.excerpt,
    image: req.body.image,
    date: postDate(),
  }
  const updatedPost = Object.assign(post, changes);
  res.send(updatedPost);
});


app.delete('/api/posts/:postId', (req, res) => { // delete post by ID
  const id = String(req.params.id);
  const post = posts.find(post => post.id === id);
  const idx = posts.indexOf(post);
  posts.splice(idx, 1);
  res.send(posts);
});

app.get('*', (req, res) => res.sendFile(path.join(__dirname, '../build', 'index.html')));
app.listen(process.env.PORT || 3000);
