const { Pool } = require('pg');
const moment = require('moment');
require('dotenv').config({ path: '../.env' });

const pool = new Pool({
  host: process.env.DB_HOST, // server name or IP address;
  port: process.env.DB_PORT, // server port
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  ssl: true,
});

function postDate() {
  return moment().format();
}

const getPostsLength = (request, response) => {
  pool
    .query(`SELECT * FROM post`)
    .then(posts => {
      response.status(200).json({ count: posts.rows.length })
    })
    .catch(err => console.error('Error executing query', err.stack))
}

const getPosts = (request, response) => {
  let { limit, offset } = request.query;
  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 6;
  offset = offset < 0 ? 0 : offset;
  limit = Math.min(50, Math.max(1, limit));
  pool
    .query(`SELECT * FROM post ORDER BY date DESC LIMIT $1 OFFSET $2`, [Number(limit), Number(offset)])
    .then(posts => {
      response.status(200).json({ posts: posts.rows })
    })
    .catch(err => console.error('Error executing query', err.stack))
}


const getPostById = (request, response) => {
  const id = parseInt(request.params.id);
  pool
    .query('SELECT * FROM post WHERE id = $1', [id])
    .then(post => response.status(200).json(post.rows[0]))
    .catch(err => console.error('Error executing query', err.stack))
}

const createPost = (request, response) => {
  let title = request.body.title;
  let excerpt = request.body.excerpt;
  let image = request.body.image;
  let date = postDate();
  pool
    .query('INSERT INTO post (title, excerpt, image, date) VALUES ($1, $2, $3, $4)', [title, excerpt, image, date])
    .then(post => response.status(201).send(`User added with ID: ${post.insertId}`))
    .catch(err => console.error('Error executing query', err.stack))
}

const updatePost = (request, response) => {
  let id = String(request.params.id);
  let id_number = Number(request.params.id);
  let title = request.body.title;
  let excerpt = request.body.excerpt;
  let image = request.body.image;
  let date = postDate();
  pool
    .query('UPDATE post SET title = $1, excerpt = $2, image = $3, date = $4 WHERE id = $5', [title, excerpt, image, date, id_number])
    .then(post => response.status(200).send(`Post modified with ID: ${id}`))
    .catch(err => console.error('Error executing query', err.stack))
}

const deletePost = (request, response) => {
  const id = parseInt(request.params.id)
  pool
    .query('DELETE FROM post WHERE id = $1', [id])
    .then(post => response.status(200).send(`User deleted with ID: ${id}`))
    .catch(err => console.error('Error executing query', err.stack))
}


module.exports = {
  getPosts,
  getPostById,
  createPost,
  updatePost,
  deletePost,
  getPostsLength,
}
