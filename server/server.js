const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const db = require('./queries');
const fetch = require('node-fetch');
app.use(express.static(path.join(__dirname, '../build')));
app.use(express.json());

app.get('/ping', function (req, res) {
  return res.send('pong');
});

const validateTokenInfo = (req, res) => {
  fetch(`https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=${req.body.token}`)
    .then(res => res.json())
    .then(json => {
      if (json.email === 'rudenkokseniya91@gmail.com') {
        json.admin = true;
      } else {
        json.admin = false;
      }
      res.send(json.admin.le);
    },
      error => {
        console.error('An error occurred', error);
      });
}

app.post('/api/tokeninfo', validateTokenInfo);
app.get('/api/posts', db.getPosts);
app.get('/api/postslength', db.getPostsLength);
app.get('/api/posts/:id', db.getPostById);
app.post('/api/posts', db.createPost);
app.patch('/api/posts/:id', db.updatePost);
app.delete('/api/posts/:id', db.deletePost);
app.get('*', (req, res) => res.sendFile(path.join(__dirname, '../build', 'index.html')));
app.listen(process.env.PORT || 3000);

