import {
  POSTS_RECEIVED,
  POSTS_FAILED,
  POST_RECEIVED,
  ADD_POST,
  POST_UPDATED,
  POST_DELETED,
  Post,
} from 'actions/asyncPosts/actionTypes';
import createReducer from 'helpers/createReducer';

export interface PostsState {
  posts: Post[];
  post: Post | null;
  error: string;
  currentPost: Post | null;
  classes: {
    [key: string]: string,
  };
  pagination: {
    hasMore: boolean;
    limit: number;
    offset: number;
    count: number
  };
}

const initialState = {
  posts: [],
  post: null,
  error: 'oops',
  currentPost: null,
  classes: {},
  pagination: {
    hasMore: true,
    limit: 6,
    offset: 0,
    count: 0,
  },
} as PostsState;

export default createReducer('posts', initialState)(
  (state = initialState, action): PostsState => {
    switch (action.type) {

      case POSTS_RECEIVED:
        return {
          ...state,
          posts: state.posts.concat(action.data.posts),
          pagination: {
            ...state.pagination,
            offset: state.pagination.offset + state.pagination.limit,
            count: action.data.pagination.count,
            hasMore: action.data.pagination.hasMore,
          },
        };
      case POST_RECEIVED:
        return { ...state, currentPost: action.post };
      case POSTS_FAILED:
        return {
          ...state,
          error: action.error,
          pagination: { ...state.pagination, hasMore: false },
        };
      case ADD_POST:
        return {
          ...state,
          posts: [action.newPost].concat(state.posts),
        };
      case POST_UPDATED:
        return {
          ...state,
          posts: [...state.posts].map((post) => {
            return post.id === action.updatedPost.id ? action.updatedPost : post;
          }),
          currentPost: action.updatedPost,
        };
      case POST_DELETED:
        return {
          ...state,
          posts: [...state.posts].filter(post => post.id !== action.id),
        };
      default:
        return state;
    }
  },
);
