import { combineReducers } from 'redux';
import posts from './posts/reducers';
import auth from './auth/reducers';
import { connectRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();
export default combineReducers({
  posts,
  auth,
  router: connectRouter(history),
});
