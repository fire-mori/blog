import { GOOGLE_LOGIN, GOOGLE_LOGOUT, GOOGLE_INITIALIZATION } from 'actions/auth/actionTypes';
import createReducer from 'helpers/createReducer';

export interface AuthState {
  email: string | null;
  admin: boolean;
}

const initialState = {
  email: null,
  admin: false,
} as AuthState;

export default createReducer('auth', initialState)(
  (state = initialState, action): AuthState => {
    switch (action.type) {
      case GOOGLE_LOGIN:
        return { ...state, email: action.email, admin: action.admin };
      case GOOGLE_LOGOUT:
        return { ...state, email: null, admin: false };
      case GOOGLE_INITIALIZATION:
        return { ...state, email: action.email, admin: action.admin };
      default:
        return state;
    }
  },
);
