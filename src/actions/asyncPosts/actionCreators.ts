import {
  PostsAsyncActions,
  POSTS_RECEIVED,
  POSTS_FAILED,
  POST_RECEIVED,
  POST_UPDATED,
  POST_DELETED,
  ADD_POST_FAILED,
  ADD_POST,
} from './actionTypes';
import ThunkAction from 'actions/ThunkAction';

export function requestPosts(offset: number, limit: number): ThunkAction<void> {
  return (dispatch) => {

    const postsRequest = fetch(`/api/posts?limit=${limit}&offset=${offset}`)
      .then(response => response.json());

    const postsLengthRequest = fetch('/api/postslength')
      .then(response => response.json());

    return Promise.all([postsRequest, postsLengthRequest])
      .then(res => {
        const obj = { ...res[0], ...res[1] };
        const posts = obj.posts;
        const count = obj.count;
        const hasMore = obj.count - (offset + limit) > 0;
        const pagination = { offset, limit, count, hasMore };
        const data = { posts, pagination };
        dispatch(receivedPosts(data));
      })
      .catch((error) => {
        dispatch(failedPosts(error));
      });
  };
}

export function receivedPosts(data: any): PostsAsyncActions {
  return {
    data,
    type: POSTS_RECEIVED,
  };
}

export function requestPost(id: string): ThunkAction<void> {
  return dispatch => {
    fetch(`/api/posts/${id}`)
      .then(res => res.json())
      .then((res) => {
        if (res.error) {
          throw res.error;
        }
        dispatch(receivedPost(res));
      })
      .catch((error) => {
        dispatch(failedPosts(error));
      });
  };
}

export function receivedPost(post: any): PostsAsyncActions {
  return {
    post,
    type: POST_RECEIVED,
  };
}

export function failedPosts(error: string): PostsAsyncActions {
  return {
    error,
    type: POSTS_FAILED,
  };
}

export function addNewPost(newPost: any): PostsAsyncActions {
  return {
    newPost,
    type: ADD_POST,
  };
}

export function saveNewPost(newPost: any): ThunkAction<void> {
  return (dispatch: any) => {
    fetch('/api/posts', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newPost),
    }).then(() => {
      dispatch(addNewPost(newPost));
    })
      .catch(err => console.log(err));
  };
}

export function failedAddPost(error: string): PostsAsyncActions {
  return {
    error,
    type: ADD_POST_FAILED,
  };
}

export function postUpdated(updatedPost: any): PostsAsyncActions {

  return {
    updatedPost,
    type: POST_UPDATED,
  };
}

export function updatePost(updatedPost: any, id: string): ThunkAction<void> {

  return (dispatch) => {
    fetch(`/api/posts/${id}`, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedPost),
    }).then(() => {
      dispatch(postUpdated(updatedPost));
    })
      .catch((error) => {
        dispatch(failedAddPost(error));
      });
  };
}

export function postDeleted(id: any) {
  return {
    id,
    type: POST_DELETED,
  };
}

export function deletePost(id: string) {
  return (dispatch: any) => {
    fetch(`/api/posts/${id}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id,
      }),
    }).then(() => {
      dispatch(postDeleted(id));
    })
      .catch(err => console.log(err));
  };
}
