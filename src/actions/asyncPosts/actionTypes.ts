import Action from '../Action';

export const POSTS_REQUESTED = 'POSTS_REQUESTED';
export const POSTS_RECEIVED = 'POSTS_RECEIVED';
export const POSTS_FAILED = 'POSTS_FAILED';
export const POST_RECEIVED = 'POST_RECEIVED ';
export const ADD_POST = 'ADD_POST';
export const POST_UPDATED = 'POST_UPDATED';
export const POST_DELETED = 'POST_DELETED';
export const ADD_POST_FAILED = 'ADD_POST_FAILED';
export const POSTS_REQUEST = 'POSTS_REQUEST';

export interface Post {
  id: string;
  title: string;
  excerpt: string;
  date: string;
  image: string;
}

export interface PostsReceived extends Action<typeof POSTS_RECEIVED> {
  data: {
    posts: Post[];
    pagination: {
      hasMore: boolean;
      limit: number;
      offset: number;
      count: number;
    };
  };
}

export interface PostsFailed extends Action<typeof POSTS_FAILED> {
  error: string;
}

export interface PostsRequested extends Action<typeof POSTS_REQUESTED> { }

export interface PostReceived extends Action<typeof POST_RECEIVED> {
  post: Post;
}
export interface AddNewPost extends Action<typeof ADD_POST> {
  newPost: Post;
}

export interface PostUpdated extends Action<typeof POST_UPDATED> {
  updatedPost: Post;
}

export interface PostDeleted extends Action<typeof POST_DELETED> {
  id: string;
}

export interface AddPostFailed extends Action<typeof ADD_POST_FAILED> {
  error: string;
}

export type PostsAsyncActions =
  | PostsReceived
  | PostReceived
  | PostsFailed
  | PostsRequested
  | PostUpdated
  | PostDeleted
  | AddNewPost
  | AddPostFailed;
