import Action from '../Action';

export const GOOGLE_LOGOUT = 'GOOGLE_LOGOUT';
export const GOOGLE_LOGIN = 'GOOGLE_LOGIN';
export const GOOGLE_INITIALIZATION = 'GOOGLE_INITIALIZATION';
export const TOKEN_INFO_REQUEST_FAILED = 'TOKEN_INFO_REQUEST_FAILED';

export interface GoogleLogin extends Action<typeof GOOGLE_LOGIN> {
  email: string;
  token: string;
  admin: boolean;
}

export interface GoogleLogout extends Action<typeof GOOGLE_LOGOUT> {
}

export interface GoogleInitialization extends Action<typeof GOOGLE_INITIALIZATION> {
  email: string | null;
  token: string | null;
  admin: boolean;
}

export interface FailedTokenInfoRequest extends Action<typeof TOKEN_INFO_REQUEST_FAILED> {
  error: string;
}

export type AuthActions = GoogleLogin
  | GoogleLogout
  | GoogleInitialization
  | FailedTokenInfoRequest;
