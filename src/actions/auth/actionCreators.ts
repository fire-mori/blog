import {
  AuthActions,
  GOOGLE_LOGIN,
  GOOGLE_LOGOUT,
  GOOGLE_INITIALIZATION,
  TOKEN_INFO_REQUEST_FAILED,
} from './actionTypes';
import ThunkAction from 'actions/ThunkAction';

export function requestTokenInfo(email: string, token: string): ThunkAction<void> {
  return (dispatch) => {
    fetch('/api/tokeninfo', {
      method: 'POST',
      body: JSON.stringify({ token }),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((res: any) => res.json())
      .then((res: any) => {
        if (res.error) {
          throw res.error;
        }
        dispatch(googleLogin(email, token, res));
      })
      .catch((error: any) => {
        dispatch(failedTokenInfoRequest(error));
      });
  };
}

export function googleLogin(email: string, token: string, admin: boolean): AuthActions {
  const isAdmin = String(admin);
  window.localStorage.setItem('token', token);
  window.localStorage.setItem('email', email);
  window.localStorage.setItem('admin', isAdmin);
  return {
    email,
    token,
    admin,
    type: GOOGLE_LOGIN,
  };
}

export function googleLogout(): AuthActions {
  window.localStorage.removeItem('token');
  window.localStorage.removeItem('email');
  window.localStorage.removeItem('admin');
  return {
    type: GOOGLE_LOGOUT,
  };
}

export function googleInitialization(): AuthActions {
  const token = window.localStorage.getItem('token');
  const email = window.localStorage.getItem('email');
  const isAdmin = window.localStorage.getItem('admin');
  const admin = (isAdmin === 'true');
  return {
    email,
    token,
    admin,
    type: GOOGLE_INITIALIZATION,
  };
}

export function failedTokenInfoRequest(error: string): AuthActions {
  return {
    error,
    type: TOKEN_INFO_REQUEST_FAILED,
  };
}
