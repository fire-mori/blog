import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestPost, deletePost } from 'actions/asyncPosts/actionCreators';
import { googleLogin } from 'actions/auth/actionCreators';
import RootState from 'store/RootState';
import { Post } from 'actions/asyncPosts/actionTypes';
import { Grid, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import CalendarToday from '@material-ui/icons/CalendarToday';
import { CSSProperties, ThemeProvider } from '@material-ui/styles';
import { createMuiTheme, withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import moment from 'moment';

interface PostDetailsProps {
  post: Post | null;
  id: string;
  classes: {
    [key: string]: string,
  };
  auth: {
    email: string | null;
    admin: boolean;
  };
}
export interface Styles {
  [key: string]: CSSProperties;
}

const styles: Styles = {
  container: {
    fontSize: 'calc(10px + 2vmin)',
  },
  button: {
    float: 'right',
  },
  date: {
    fontSize: '0.9rem',
    opacity: 0.5,
  },
  iconCalendar: {
    fontSize: '0.9rem',
    opacity: 0.5,
    marginRight: '4px',
    position: 'relative',
    top: 2,
  },
  postTitle: {
    marginTop: 10,
  },
  postExcerpt: {
    maxWidth: 344,
    textAlign: 'left',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  cardActions: {
    justifyContent: 'space-between',
  },
  card: {
    maxWidth: 1100,
    textAlign: 'left',
    margin: 'auto',
    width: 1100,
  },
};

const themePosts = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#3f51b5' },
    secondary: { main: '#61dafb' },
  },
});

class PostDetails extends React.Component<
  PostDetailsProps & typeof dispatchProps
  > {
  componentDidMount() {
    this.props.requestPost(this.props.id);
  }

  deleteMember = (id: string) => {
    this.props.deletePost(id);
  }

  render() {
    const { classes } = this.props;
    const { admin } = this.props.auth;

    if (!this.props.post) {
      return <div>Loading...</div>;
    }
    return (
      <ThemeProvider theme={themePosts}>
        <div style={{ marginTop: 20, padding: 30, margin: 'auto' }}>
          <Grid container spacing={3} justify="center">
            <Grid item key={this.props.post.title}>
              <Card className={classes.card}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    alt="Doge"
                    height="580"
                    image={this.props.post.image}
                    title="Doge"
                  />
                  <CardContent>
                    <Typography component="p" className={classes.date}>
                      <CalendarToday className={classes.iconCalendar} />
                      {moment(this.props.post.date).format('DD/MM/YYYY')}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="h2"
                      className={classes.title}
                    >
                      {this.props.post.title}
                    </Typography>
                    <Typography component="p">
                      {this.props.post.excerpt}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions className={classes.cardActions}>
                  {admin && (<div className={classes.container}>
                    <Button
                      size="small"
                      color="primary"
                      className={classes.button}
                      onClick={() => { this.deleteMember(this.props.id); }}
                    >
                      Delete
                      </Button>
                    <Button
                      component={Link}
                      to={`/edit-post/${this.props.id}`}
                      size="small"
                      color="primary"
                      className={classes.button}
                    >
                      Edit
                      </Button>
                  </div>)}
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </div>
      </ThemeProvider>
    );
  }
}

const dispatchProps = {
  googleLogin,
  requestPost,
  deletePost,
};

export default connect(
  (state: RootState, props: any) => {
    return {
      post: state.posts.currentPost,
      posts: state.posts,
      id: props.match.params.id as string,
      auth: state.auth,
    };
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(withStyles(styles)(PostDetails));

