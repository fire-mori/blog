import { RouteComponentProps } from '@reach/router';
import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { injectIntl, IntlShape } from 'react-intl';
import { connect } from 'react-redux';
import Posts from 'components/Home/Posts';
import Avatar from 'components/Home/Avatar';
import { bindActionCreators } from 'redux';
import { googleInitialization } from 'actions/auth/actionCreators';
import AddPostButton from 'components/Home/AddPostButton';
import {
  createMuiTheme,
  createStyles,
  makeStyles,
} from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import RootState from 'store/RootState';

export interface HomeProps extends RouteComponentProps { }

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      textAlign: 'center',
    },
    header: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 'calc(10px + 2vmin)',
    },
  }),
);

const themePosts = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#3f51b5' },
    secondary: { main: '#61dafb' },
  },
});

interface authProps {
  email: string | null;
  admin: boolean;
}

const Home: React.FC<HomeProps & { intl: IntlShape } & authProps & typeof dispatchProps> = (props) => {

  useEffect(() => {
    props.googleInitialization();
  });

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Helmet>
        <title>
          {props.intl.formatMessage({
            id: 'nav.home',
            defaultMessage: 'Home',
          })}
        </title>
      </Helmet>
      <div className={classes.header}>
        <ThemeProvider theme={themePosts}>
          <Avatar></Avatar>
          <Posts></Posts>
        </ThemeProvider>
      </div>
      {props.admin && <AddPostButton></AddPostButton>}
    </div>
  );
};

const dispatchProps = {
  googleInitialization,
};

export default connect<void, void, void, RootState>(
  (state: RootState) => {
    return state.auth;
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(injectIntl(Home));
