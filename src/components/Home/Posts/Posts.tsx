import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import CalendarToday from '@material-ui/icons/CalendarToday';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestPosts, deletePost } from 'actions/asyncPosts/actionCreators';
import RootState from 'store/RootState';
import { Post } from 'actions/asyncPosts/actionTypes';
import { googleLogin } from 'actions/auth/actionCreators';
import { Link } from 'react-router-dom';
import { history } from 'reducers/index';
import { CSSProperties } from '@material-ui/styles';
import moment from 'moment';
import InfiniteScroll from 'react-infinite-scroll-component';
export interface Styles {
  [key: string]: CSSProperties;
}

const styles: Styles = {
  container: {
    fontSize: 'calc(10px + 2vmin)',
  },
  button: {
    float: 'right',
  },
  date: {
    fontSize: '0.9rem',
    opacity: 0.5,
  },
  iconCalendar: {
    fontSize: '0.9rem',
    opacity: 0.5,
    marginRight: '4px',
    position: 'relative',
    top: 2,
  },
  postTitle: {
    marginTop: 10,
  },
  postExcerpt: {
    maxWidth: 344,
    textAlign: 'left',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  cardActions: {
    justifyContent: 'space-between',
  },
  card: {
    maxWidth: 344,
    textAlign: 'left',
  },
};

interface PostsAndAuthState {
  posts: {
    posts: Post[];
    error: string;
    currentPost: Post | null;
    pagination: {
      hasMore: boolean;
      limit: number;
      offset: number;
      count: number;
    };
  };
  classes: {
    [key: string]: string,
  };
  auth: {
    email: string | null;
    admin: boolean;
  };
}

class Posts extends React.Component<PostsAndAuthState & typeof dispatchProps> {
  componentWillMount() {
    const { offset, limit } = this.props.posts.pagination;
    this.props.requestPosts(offset, limit);
  }

  loadMore() {
    const { offset, limit } = this.props.posts.pagination;
    this.props.requestPosts(offset, limit);
  }

  deleteMember = (id: string) => {
    const { offset, limit } = this.props.posts.pagination;
    this.props.deletePost(id);
    this.props.requestPosts(offset, limit);
  }
  render() {
    const { classes } = this.props;
    const { hasMore } = this.props.posts.pagination;
    const { admin } = this.props.auth;
    return (
      <div style={{ marginTop: 20, padding: 30, maxWidth: 1200 }}>
        <InfiniteScroll
          style={{ overflowX: 'hidden' }}
          dataLength={this.props.posts.posts.length}
          next={() => { this.loadMore(); }}
          hasMore={hasMore}
          loader={<h5>Loading...</h5>}
          endMessage={
            <p style={{ textAlign: 'center' }}>
              <b>Yay! You have seen it all</b>
            </p>
          }
        >
          <Grid container spacing={4} justify="center">
            {this.props.posts.posts.map((post: Post) => (
              <Grid item key={post.title}>
                <Card
                  className={classes.card}
                >
                  <CardActionArea
                    onClick={() => {
                      history.push(`/post/${post.id}`);
                    }}
                  >
                    <CardMedia
                      component="img"
                      alt="Doge"
                      height="240"
                      image={post.image}
                      title="Doge"
                    />
                    <CardContent>
                      <Typography component="p" className={classes.date}>
                        <CalendarToday className={classes.iconCalendar} />
                        {moment(post.date).format('DD/MM/YYYY')}
                      </Typography>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="h2"
                        className={classes.postTitle}
                      >
                        {post.title}
                      </Typography>
                      <Typography component="p" className={classes.postExcerpt}>
                        {post.excerpt}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions className={classes.cardActions}>
                    <Button
                      component={Link}
                      to={`/post/${post.id}`}
                      size="small"
                      color="primary"
                    >
                      Learn More
                  </Button>
                    {admin && (<div className={classes.container}>
                      <Button
                        size="small"
                        color="primary"
                        className={classes.button}
                        onClick={() => { this.deleteMember(post.id); }}
                      >
                        Delete
                      </Button>
                      <Button
                        component={Link}
                        to={`/edit-post/${post.id}`}
                        size="small"
                        color="primary"
                        className={classes.button}
                      >
                        Edit
                      </Button>
                    </div>)}
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </InfiniteScroll>
      </div>
    );
  }
}

const dispatchProps = {
  requestPosts,
  googleLogin,
  deletePost,
};

export default connect(
  (state: RootState) => {
    return {
      posts: state.posts,
      auth: state.auth,
    };
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(withStyles(styles)(Posts));
