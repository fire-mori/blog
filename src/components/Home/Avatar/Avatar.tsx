import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(() =>
  createStyles({
    bigAvatar: {
      marginTop: 80,
      width: 80,
      height: 80,
    },
    persona: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 'calc(10px + 2vmin)',
    },
  }),
);

export default function ImageAvatars() {
  const classes = useStyles();

  return (
    <Grid container justify="center" alignItems="center">
      <div className={classes.persona}>
        <Avatar
          alt="Kseniya Rudenko"
          src="https://avatars1.githubusercontent.com/u/51728799?s=460&v=4"
          className={classes.bigAvatar}
        />
        <p>Kseniya Rudenko</p>
      </div>
    </Grid>
  );
}
