import { RouteComponentProps } from '@reach/router';
import React, { FC } from 'react';
import { RawIntlProvider } from 'react-intl';
import Nav from 'components/Nav';
import Menu from 'components/Menu';
import { createIntl } from 'helpers/intl';
import Locale from 'models/Locale';
import AppTheme from 'themes/AppTheme';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  '@global': {
    body: {
      margin: '0',
      padding: '0',
      fontFamily:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
      '-webkit-font-smoothing': 'antialiased',
      '-moz-osx-font-smoothing': 'grayscale',
      backgroundColor: '#f8f8f2',
      color: '#282a36',
    },

    a: {
      color: '#61dafb',
    },

    code: {
      fontFamily:
        "source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace",
    },
  },
};

export interface LayoutProps extends RouteComponentProps {
  locale?: Locale;
}

const Layout: FC<LayoutProps> = ({ children, locale = 'en' }) => {
  AppTheme.context().useLayoutEffect({
    classNames: ['@global'],
  });

  return (
    <RawIntlProvider value={createIntl(locale)}>
      <Nav />
      <Menu />
      {children}
    </RawIntlProvider>
  );
};

export default withStyles(styles)(Layout);
