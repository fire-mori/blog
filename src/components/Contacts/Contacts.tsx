import { RouteComponentProps } from '@reach/router';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { injectIntl, IntlShape } from 'react-intl';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      textAlign: 'center',
    },
    header: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 'calc(10px + 2vmin)',
    },
  }),
);

export interface ContactProps extends RouteComponentProps {}

const Contacts: React.FC<ContactProps & { intl: IntlShape }> = props => {
  const classes = useStyles();
  const title = props.intl.formatMessage({
    id: 'contacts',
    defaultMessage: 'Contacts',
  });
  return (
    <div className={classes.root}>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <div className={classes.header}>{title}</div>
    </div>
  );
};

export default injectIntl(Contacts);
