import React from 'react';
import GlobalErrorBoundary from 'components/GlobalErrorBoundary';
import Routes from 'components/Routes';
import { googleInitialization } from '../../actions/auth/actionCreators';
import RootState from 'store/RootState';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
interface AuthProps {
  email: string | null;
}

export class App extends React.Component<AuthProps & typeof dispatchProps> {

  componentDidMount() {
    this.props.googleInitialization();
  }

  render() {
    return (
      <GlobalErrorBoundary>
        <Routes />
      </GlobalErrorBoundary>
    );
  }
}

const dispatchProps = {
  googleInitialization,
};

export default connect(
  (state: RootState) => {
    return state.auth;
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(App);
