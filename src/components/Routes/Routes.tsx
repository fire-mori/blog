import React, { FC } from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Route } from 'react-router-dom'; // BrowserRouter as Router,
import About from 'components/About';
import Home from 'components/Home';
import AddPost from 'components/AddPost';
import Contacts from 'components/Contacts';
import Layout from 'components/Layout';
import PostDetails from 'components/PostDetails';
import { history } from '../../reducers';
import EditPost from 'components/EditPost';

const Routes: FC = () => {
  return (
    <ConnectedRouter history={history}>
      <Layout>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contacts" component={Contacts} />
        <Route path="/new-post" component={AddPost} />
        <Route path="/edit-post/:id" component={EditPost} />
        <Route path="/post/:id" component={PostDetails} />
      </Layout>
    </ConnectedRouter>
  );
};

export default Routes;
