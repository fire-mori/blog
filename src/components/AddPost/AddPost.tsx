import React from 'react';
import { FormControl, InputLabel, Input, Button, createMuiTheme } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { CSSProperties } from '@material-ui/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { saveNewPost } from 'actions/asyncPosts/actionCreators';
import RootState from 'store/RootState';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { history } from 'reducers/index';
import { ThemeProvider } from '@material-ui/styles';

export interface Post {
  id: string;
  title: string;
  excerpt: string;
  date: string;
  image: string;
}
interface PostDetailsProps {
  post: Post | null;
  classes: {
    [key: string]: string,
  };
}
export interface Styles {
  [key: string]: CSSProperties;
}

const styles: Styles = {
  image: {
    width: '100%',
    height: '100%',
  },
};

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#3f51b5' },
    secondary: { main: '#61dafb' },
  },
});

function getBase64(file: any, callback: any) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(file);
}

class AddPost extends React.Component<PostDetailsProps & typeof dispatchProps> {

  state = {
    title: '',
    excerpt: '',
    image: '',
    date: {},
    open: false,
  };

  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  setData = () => {
    const today = new Date();
    this.setState({
      date: today,
    });
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  }

  handleClose = () => {
    this.setState({
      open: false,
    });
  }

  handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target;
    const files = input.files;
    if (files != null) {
      const file = files[0];
      getBase64(file, (base64Data: string) => {
        this.setState({
          image: base64Data,
        });
      });
    }
  }

  handleSubmit = (event: React.MouseEvent) => {
    event.preventDefault();
    this.props.saveNewPost(this.state);
    history.push('/');
  }

  render() {
    const { classes } = this.props;
    const { title, excerpt, image } = this.state;
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          margin: 20,
          padding: 20,
        }}
      >
        <form style={{ width: '100%', maxWidth: 1100 }}>
          <h1>New post</h1>

          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="tittle">Tittle</InputLabel>
            <Input
              onChange={this.handleInputChange}
              id="title"
              name="title"
              type="text"
              value={title} />
          </FormControl>

          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="body">Body</InputLabel>
            <Input
              onChange={this.handleInputChange}
              id="body"
              name="excerpt"
              multiline rows={10}
              value={excerpt} />
          </FormControl>

          <FormControl margin="normal" fullWidth >
            <Input type="file" onChange={this.handleFileChange} />
            {image && <img
              className={classes.image}
              src={image}
              alt="uploaded" />}
          </FormControl>

          <Button onClick={this.handleClickOpen} variant="contained" color="primary" size="medium">
            Send
          </Button>
        </form>
        <ThemeProvider theme={theme}>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{'Add new post?'}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to continue?
          </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Back to editing
          </Button>
              <Button onClick={this.handleSubmit} color="primary" autoFocus>
                Add new post
          </Button>
            </DialogActions>
          </Dialog>
        </ThemeProvider>
      </div>
    );
  }
}

const dispatchProps = {
  saveNewPost,
};

export default connect(
  (state: RootState) => {
    return state.posts;
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(withStyles(styles)(AddPost));
