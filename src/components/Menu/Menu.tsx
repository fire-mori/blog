import React from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { ThemeProvider } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RootState from 'store/RootState';
import { RouterState } from 'connected-react-router';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: { main: '#61dafb' },
    secondary: { main: '#11cb5f' },
  },
});

const bgStyle = {
  backgroundColor: '#3f51b5',
  borderRadius: '0',
  boxShadow:
    '0px 3px 3px 0px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 2px 1px -1px rgba(0,0,0,0.12)',
};

export function CenteredTabs(props: RouterState & typeof dispatchProps) {
  const classes = useStyles();
  let pathName = false as boolean | string;
  const isUrlValid = ['/', '/contacts', '/about'].includes(
    props.location.pathname,
  );
  if (isUrlValid) {
    pathName = props.location.pathname;
  }
  return (
    <ThemeProvider theme={theme}>
      <Paper className={classes.root} style={bgStyle}>
        <Tabs
          value={pathName}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab value="/" label="Home" component={Link} to="/" />
          <Tab value="/about" label="About" component={Link} to="/about" />
          <Tab
            value="/contacts"
            label="Contacts"
            component={Link}
            to="/contacts"
          />
        </Tabs>
      </Paper>
    </ThemeProvider>
  );
}

const dispatchProps = {};

export default connect(
  (state: RootState) => {
    return state.router;
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(CenteredTabs);
