import React from 'react';
import { createStyles, makeStyles, Theme, createMuiTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { googleLogin, googleLogout, requestTokenInfo } from '../../actions/auth/actionCreators';
import { GoogleLogout, GoogleLogin } from 'react-google-login';
import RootState from 'store/RootState';
import { ThemeProvider } from '@material-ui/styles';
import { Link } from 'react-router-dom';
interface AuthProps {
  email: string | null;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    button: {
      backgroundColor: '#61dafb',
    },
  }),
);

const themeButtons = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#61dafb' },
    secondary: { main: '#11cb5f' },
  },
});

export function ButtonAppBar(props: AuthProps & typeof dispatchProps) {
  const classes = useStyles();

  const getEmail = (response: any) => {
    props.requestTokenInfo(
      response.profileObj.email,
      response.Zi.access_token,
    ); // dispatch = trigger
  };

  const removeEmail = () => {
    props.googleLogout();
  };

  const renderComp = () => {
    if (props.email) {
      return (<GoogleLogout
        clientId={'924731206118-6o8kvg32c40gigmas51asgjj85h2efdl.apps.googleusercontent.com'}
        render={renderProps => (
          <ThemeProvider theme={themeButtons}>
            <Button
              className={classes.button}
              onClick={() => { renderProps.onClick(); removeEmail(); }}
              disabled={renderProps.disabled}>
              Logout
            </Button>
          </ThemeProvider>
        )}
        buttonText="Logout"
      // onLogoutSuccess={removeEmail}

      >
      </GoogleLogout>);
    }
    return (<GoogleLogin
      clientId={'924731206118-6o8kvg32c40gigmas51asgjj85h2efdl.apps.googleusercontent.com'}
      render={renderProps => (
        <ThemeProvider theme={themeButtons}>
          <Button
            className={classes.button}
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}>
            Login with Google
          </Button>
        </ThemeProvider>
      )}
      onSuccess={getEmail}
      onFailure={getEmail}
    >
      <span> Login with Google</span>
    </GoogleLogin>);

  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="home"
            component={Link}
            to="/"
          >
            <CreateIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Blog {props.email}
          </Typography>
          {renderComp()}
        </Toolbar>
      </AppBar>
    </div>
  );
}

const dispatchProps = {
  googleLogin,
  googleLogout,
  requestTokenInfo,
};

export default connect(
  (state: RootState) => {
    return state.auth;
  },
  dispatch => bindActionCreators(dispatchProps, dispatch), // function pass action to reducer
)(ButtonAppBar);
