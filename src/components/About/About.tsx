import { RouteComponentProps } from '@reach/router';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { injectIntl, IntlShape, FormattedMessage } from 'react-intl';
import logoSvg from './logo.svg';
import Button from '@material-ui/core/Button';
export interface AboutProps extends RouteComponentProps {}

const useStyles = makeStyles(() =>
  createStyles({
    '@keyframes spin': {
      from: { transform: 'rotate(0deg)' },
      to: { transform: 'rotate(360deg)' },
    },
    logo: {
      animation: '$spin infinite 20s linear',
      height: '40vmin',
      pointerEvents: 'none',
    },
    root: {
      textAlign: 'center',
    },
    header: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 'calc(10px + 2vmin)',
    },
  }),
);

const About: React.FC<AboutProps & { intl: IntlShape }> = props => {
  const classes = useStyles();
  const title = props.intl.formatMessage({
    id: 'about',
    defaultMessage: 'About',
  });
  return (
    <div className={classes.root}>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <div className={classes.header}>
        <img className={classes.logo} src={logoSvg} alt="logo" />
        <p>Edit src/components/Home/Home.tsx and save to reload.</p>
        <Button variant="contained" color="primary">
          <FormattedMessage id="home.learn" defaultMessage="Learn React" />
        </Button>
      </div>
    </div>
  );
};

export default injectIntl(About);
