import React from 'react';
import { FormControl, InputLabel, Input, Button, createMuiTheme } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { CSSProperties } from '@material-ui/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatePost, requestPost, postUpdated } from 'actions/asyncPosts/actionCreators';
import RootState from 'store/RootState';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { history } from 'reducers/index';
import { ThemeProvider } from '@material-ui/styles';

export interface Post {
  id: string;
  title: string;
  excerpt: string;
  date: string;
  image: string;
}
interface PostDetailsProps {
  post: Post | null;
  id: string;
  classes: {
    [key: string]: string,
  };
}
export interface Styles {
  [key: string]: CSSProperties;
}

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#3f51b5' },
    secondary: { main: '#61dafb' },
  },
});

const styles: Styles = {
  image: {
    width: '100%',
    height: '100%',
  },
};

function getBase64(file: any, callback: any) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(file);
}

class EditPost extends React.Component<PostDetailsProps & typeof dispatchProps> {

  componentDidMount() {
    this.props.requestPost(this.props.id);
  }

  state = {
    id: this.props.post ? this.props.post.id : '',
    title: this.props.post ? this.props.post.title : '',
    excerpt: this.props.post ? this.props.post.excerpt : '',
    image: this.props.post ? this.props.post.image : '',
    open: false,
  };

  componentWillReceiveProps = (nextProps: PostDetailsProps) => {
    if (nextProps.post) {
      this.setState({
        id: nextProps.post.id,
        title: nextProps.post.title,
        excerpt: nextProps.post.excerpt,
        image: nextProps.post.image,
      });
    }
  }

  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target;
    const files = input.files;
    if (files != null) {
      const file = files[0];
      getBase64(file, (base64Data: string) => {
        this.setState({
          image: base64Data,
        });
      });
    }
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  }

  handleClose = () => {
    this.setState({
      open: false,
    });
  }

  handleSubmit = (event: React.MouseEvent) => {
    event.preventDefault();
    this.props.updatePost(this.state, this.props.id);
    history.push('/');
  }

  render() {
    const { classes } = this.props;
    const { title, excerpt, image } = this.state;
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          margin: 20,
          padding: 20,
        }}
      >
        <form style={{ width: '100%', maxWidth: 1100 }}>
          <h1>Edit post</h1>

          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="tittle">Tittle</InputLabel>
            <Input
              onChange={this.handleInputChange}
              id="title"
              name="title"
              type="text"
              value={title} />
          </FormControl>

          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="body">Body</InputLabel>
            <Input
              onChange={this.handleInputChange}
              id="excerpt"
              name="excerpt"
              multiline rows={10}
              value={excerpt} />
          </FormControl>

          <FormControl margin="normal" fullWidth >
            <Input type="file" onChange={this.handleFileChange} />
            <img
              className={classes.image}
              src={image}
              alt="uploaded" />
          </FormControl>

          <Button onClick={this.handleClickOpen} variant="contained" color="primary" size="medium">
            Send
          </Button>
        </form>
        <ThemeProvider theme={theme}>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{'Save the changes?'}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure you want to save the changes?
          </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Back to editing
          </Button>
              <Button onClick={this.handleSubmit} color="primary" autoFocus>
                Save
          </Button>
            </DialogActions>
          </Dialog>
        </ThemeProvider>
      </div>
    );
  }
}

const dispatchProps = {
  updatePost,
  requestPost,
  postUpdated,
};

export default connect(
  (state: RootState, props: any) => {
    return {
      posts: state.posts,
      post: state.posts.currentPost,
      id: props.match.params.id as string,
    };
  },
  dispatch => bindActionCreators(dispatchProps, dispatch),
)(withStyles(styles)(EditPost));
